﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShootTypes {

	/// <summary>
	/// 
	/// </summary>
	/// <param name="position"></param>
	/// <param name="rotation"></param>
	public static void SimpleShoot(GameObject bulletPrefab, Transform gun)
    {
        //istanzia un proiettile nella posizione dello shooter
        Rigidbody shoot;
        shoot = Object.Instantiate(bulletPrefab, gun.position, Quaternion.identity).GetComponent<Rigidbody>();
		shoot.rotation = Quaternion.Euler(gun.eulerAngles.x , gun.eulerAngles.y, gun.eulerAngles.z+180);
    }


	/// <summary>
	/// 
	/// </summary>
	/// <param name="x"></param>
	/// <param name="angle"></param>
	/// <param name="gun"></param>
    public static void ConeShoot(GameObject bulletPrefab, int x, float angle, Transform gun)
    {
        float slice;
        angle = Mathf.Clamp(angle, 0, 360);
        if (angle == 0)
        {
			SimpleShoot(bulletPrefab, gun);
        }
        else
        {
            slice = angle / x;
            for(int i =0; i<x; i++)
            {
                Rigidbody shoot;
                float shootangle = 180 - angle / 2 + slice * (i)+slice/2;
                shoot = Object.Instantiate(bulletPrefab, gun.position, Quaternion.identity).GetComponent<Rigidbody>();
                shoot.rotation = Quaternion.Euler(gun.eulerAngles.x + 0, gun.eulerAngles.y + 0, gun.eulerAngles.z + shootangle);
            }
        }
    }


}