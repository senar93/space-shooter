﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ShootingScript : MonoBehaviour {
	public ShootTypeEnum shootType;
	public GameObject bulletPrefab;

	public Timer shootCD;

	public int shootNumber = 4;
	public float shootAngleDispersion = 360;

    //public ShootTypes shoot;

	public Transform spawnPoint;

	// Use this for initialization
	void Start () {
		if(spawnPoint == null) {
			spawnPoint = this.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(shootCD.Check(true))
        {
			switch(shootType) {
				case ShootTypeEnum.SimpleShoot:
					//non va come dovrebbe andare
					ShootTypes.SimpleShoot(bulletPrefab,spawnPoint);
					break;
				case ShootTypeEnum.ConeShoot:
					ShootTypes.ConeShoot(bulletPrefab, shootNumber, shootAngleDispersion, spawnPoint);
					break;
			}
        }
	}


}




public enum ShootTypeEnum {
	SimpleShoot = 1,
	ConeShoot = 2,

	NotSet = 0
}




[CustomEditor(typeof(ShootingScript))]
[CanEditMultipleObjects]
public class ShootingScriptEditor : Editor {
	public static bool useCustomInspector = true;

	private ShootingScript objectShootingScript;

	private SerializedProperty sp_shootType;
	private SerializedProperty sp_bulletPrefab;
	private SerializedProperty sp_spawnPoint;
	private SerializedProperty sp_shootCD;
	private SerializedProperty sp_shootNumber;
	private SerializedProperty sp_shootAngleDispersion;


	public void OnEnable() {
		objectShootingScript = (ShootingScript)target;
		sp_shootType = serializedObject.FindProperty("shootType");
		sp_bulletPrefab = serializedObject.FindProperty("bulletPrefab");
		sp_spawnPoint = serializedObject.FindProperty("spawnPoint");
		sp_shootCD = serializedObject.FindProperty("shootCD");
		sp_shootNumber = serializedObject.FindProperty("shootNumber");
		sp_shootAngleDispersion = serializedObject.FindProperty("shootAngleDispersion");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		useCustomInspector = EditorGUILayout.ToggleLeft("Use Custom Inspector", useCustomInspector);
		if (useCustomInspector) {
			EditorGUILayout.PropertyField(sp_shootType);
			if(objectShootingScript.shootType != ShootTypeEnum.NotSet) {
				switch (objectShootingScript.shootType) {
					case ShootTypeEnum.SimpleShoot:
						EditorGUILayout.PropertyField(sp_bulletPrefab);
						EditorGUILayout.PropertyField(sp_spawnPoint);
						EditorGUILayout.PropertyField(sp_shootCD);
						break;
					case ShootTypeEnum.ConeShoot:
						EditorGUILayout.PropertyField(sp_bulletPrefab);
						EditorGUILayout.PropertyField(sp_spawnPoint);
						EditorGUILayout.PropertyField(sp_shootNumber);
						EditorGUILayout.PropertyField(sp_shootAngleDispersion);
						EditorGUILayout.PropertyField(sp_shootCD);
						break;
					case ShootTypeEnum.NotSet:
						break;
				}
			}
		}
		else {
			base.OnInspectorGUI();
		}

		serializedObject.ApplyModifiedProperties();
	}

}