﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
	public MovementTypeEnum movementType;

    public float speed = 5f;
    private float timeStamp = 0f;
    public float changeTime = 2f;
    public int dir = 1;
    public bool canChange = true;


    public float omega = 10f;
    public float sideSpeed = 2f;
    public float sineRange = 3f;
    // Use this for initialization



    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch(movementType) {
			case MovementTypeEnum.Simple:
				SimpleMovement();
				break;
			case MovementTypeEnum.Moveback:
				Moveback();
				break;
			case MovementTypeEnum.MovebackLoop:
				MovebackLoop();
				break;
			case MovementTypeEnum.SinCos:
				SinCos();
				break;
		}
    }


    void SimpleMovement()
    {
        this.GetComponent<Rigidbody>().velocity = this.transform.up * speed;
    }

    void Moveback()
    {
        if(timeStamp == 0f) timeStamp = Time.time;
        if ((timeStamp + changeTime< Time.time )&& canChange)
        {
            canChange = false;
            dir = -1 * dir;
            
        }
        this.GetComponent<Rigidbody>().velocity = dir * this.transform.up * speed;
    }

    void MovebackLoop()
    {
        if (timeStamp == 0f) timeStamp = Time.time;
        if (timeStamp + changeTime < Time.time)
        {
            dir = -1 * dir;
            timeStamp = Time.time;
        }
        this.GetComponent<Rigidbody>().velocity = dir * this.transform.up * speed;
    }

    void SinCos()
    {
        float sideSpeed = 2f;
        this.GetComponent<Rigidbody>().velocity = this.transform.up * speed + sineRange*this.transform.right*sideSpeed*Mathf.Cos(omega*Time.time);
    }

}

public enum MovementTypeEnum {
	Simple = 0,
	Moveback = 1,
	MovebackLoop = 2,
	SinCos = 3
}