﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;


public class ObjectRotationScript : MonoBehaviour {
	public RotationTypeEnum rotationType;

	/// <summary>
	/// gradi / secondi
	/// </summary>
	public float rotSpeed = 40f;

	public Timer rotationTimer;
	/*public float rotationChange = 5f;
    private float timeStamp = 0;*/
	

    // Update is called once per frame
    void Update()
    {
        switch (rotationType)
        {
            case RotationTypeEnum.Simple:
                SimpleRotation();
                break;
            case RotationTypeEnum.ChangeByTime:
                ChangeRotation();
                break;
            case RotationTypeEnum.NoRotation:
                break;
        }
    }

    void SimpleRotation()
    {
        transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
    }

    void ChangeRotation()
    {
        if (/*timeStamp < Time.time*/ rotationTimer.Check(true))
        {
            //timeStamp = Time.time + rotationChange;
            rotSpeed = -1 * rotSpeed;
        }
        transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
    }

}




public enum RotationTypeEnum {
	Simple = 1,
	ChangeByTime = 2,

	NoRotation = 0
}




[CustomEditor(typeof(ObjectRotationScript))] [CanEditMultipleObjects]
public class ObjectRotationEditor : Editor {
	public static bool useCustomInspector = true;

	private ObjectRotationScript objectRotationScript;

	private SerializedProperty sp_rotationType;
	private SerializedProperty sp_rotationSpeed;
	private SerializedProperty sp_rotationTimeLimit;

	public void OnEnable() {
		objectRotationScript = (ObjectRotationScript)target;
		sp_rotationType = serializedObject.FindProperty("rotationType");
		sp_rotationSpeed = serializedObject.FindProperty("rotSpeed");
		sp_rotationTimeLimit = serializedObject.FindProperty("rotationTimer");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		useCustomInspector = EditorGUILayout.ToggleLeft("Use Custom Inspector", useCustomInspector);
		if(useCustomInspector) {
			EditorGUILayout.PropertyField(sp_rotationType);
			switch (objectRotationScript.rotationType) {
				case RotationTypeEnum.Simple:
					EditorGUILayout.PropertyField(sp_rotationSpeed);
					break;
				case RotationTypeEnum.ChangeByTime:
					EditorGUILayout.PropertyField(sp_rotationSpeed);
					EditorGUILayout.PropertyField(sp_rotationTimeLimit, true);
					break;
				case RotationTypeEnum.NoRotation:
					//EditorGUILayout.HelpBox("L'oggetto non ruota", MessageType.Info);
					break;
			}
		}
		else {
			base.OnInspectorGUI();
		}

		serializedObject.ApplyModifiedProperties();
	}

}