﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRayScript : MonoBehaviour {

	public LineRenderer lineRendere;
	public DealDamageByRaycastScript raycastScript;
	public Transform lineEnd;


	// Use this for initialization
	void Start () {
		lineRendere.SetPosition(0, raycastScript.startOfLine.position);
		lineRendere.useWorldSpace = true;
	}
	
	// Update is called once per frame
	void Update () {
		raycastScript.direction = lineEnd.transform.position;
		lineRendere.SetPosition(1, lineEnd.transform.position);
		raycastScript.rayCastLenght = Vector3.Distance(raycastScript.startOfLine.position, lineEnd.transform.position);
	}
}
