﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// fa spawnare gli oggetti contenuti in spawnInfoList appena il loro timer diventa true, e poi li rimuove della lista
/// </summary>
public class SpawnScript_Type1 : MonoBehaviour {

	public bool sortListAtStart = true;

	public List<SpawnInfo> spawnInfoList;


	/// <summary>
	/// setta i timer e ordina la lista se sortListAtStart = true
	/// </summary>
	void Start() {
		//setta il timer con anche la parte casuale
		for(int i = 0; i < spawnInfoList.Count; i++) {
			spawnInfoList[i].timer.Reset();
		}

		if(sortListAtStart) {
			SortList();
		}

	}

	/// <summary>
	/// spawna e rimuove gli oggetti dalla lista nel tempo corretto
	/// </summary>
	void Update () {
		for(int i = 0; i < spawnInfoList.Count && spawnInfoList[i].timer.Check(false); spawnInfoList.RemoveAt(i)) {
			if (spawnInfoList[i].positon != null && spawnInfoList[i].prefab != null) {
				GameObject tmpEnemy = Instantiate(spawnInfoList[i].prefab, spawnInfoList[i].positon.position, spawnInfoList[i].positon.rotation);
				MovementScript tmpMovement = tmpEnemy.GetComponent<MovementScript>();
				if(tmpMovement != null && (spawnInfoList[i].customScaleX != 0 || spawnInfoList[i].customScaleY != 0)) {
					tmpMovement.xScale = spawnInfoList[i].customScaleX;
					tmpMovement.yScale = spawnInfoList[i].customScaleY;
				}
			}
		}
	}



	/// <summary>
	/// ordina la lista in ordine crescente in base al tempo complessivo del timer
	/// </summary>
	protected void SortList() {
		int n = spawnInfoList.Count - 1;
		int lastChange = n;
		while (lastChange > 0) {
			lastChange = 0;
			for (int i = 0; i < n; i++) {
				if ((spawnInfoList[i].timer.setPart + spawnInfoList[i].timer.lastTime + spawnInfoList[i].timer.randomPart) >
				    (spawnInfoList[i + 1].timer.setPart + spawnInfoList[i + 1].timer.lastTime + spawnInfoList[i + 1].timer.randomPart))
				{
					SpawnInfo spiTmp = spawnInfoList[i];
					spawnInfoList[i] = spawnInfoList[i+1];
					spawnInfoList[i+1] = spiTmp;
					lastChange = i;
				}
			}
			n = lastChange;
		}
	}

}