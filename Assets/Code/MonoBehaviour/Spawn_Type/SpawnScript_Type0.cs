﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// continua a far spawnare l'oggetto descritto da spawnInfo ogni volta che il suo timer scade
/// </summary>
public class SpawnScript_Type0 : MonoBehaviour {

	public SpawnInfo spawnInfo;

	// Use this for initialization
	void Start () {
		if (spawnInfo.positon == null)
			spawnInfo.positon = this.transform;
		if (spawnInfo.prefab == null)
			this.enabled = false;

		spawnInfo.timer.Reset();
	}
	
	// Update is called once per frame
	void Update () {
		if (spawnInfo.timer.Check(true)) {
			GameObject tmpEnemy = Instantiate(spawnInfo.prefab, spawnInfo.positon.position, spawnInfo.positon.rotation);
			MovementScript tmpMovement = tmpEnemy.GetComponent<MovementScript>();
			if (tmpMovement != null && (spawnInfo.customScaleX != 0 || spawnInfo.customScaleY != 0)) {
				tmpMovement.xScale = spawnInfo.customScaleX;
				tmpMovement.yScale = spawnInfo.customScaleY;
			}
		}
	}

}
