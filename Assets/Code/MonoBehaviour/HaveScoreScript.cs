﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaveScoreScript : MonoBehaviour {

	public float score;
	public LevelManagerScript levelManager;
	//public HealthScript objectHp;
	private bool scoreAddedFlag = false;

	void Start() {
		if(levelManager == null) {
			levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManagerScript>();
		}
		/*if (objectHp == null) {
			objectHp = this.GetComponent<HealthScript>();
		}*/
		scoreAddedFlag = false;
	}

	public void AddScore() {
		if(!scoreAddedFlag) {
			scoreAddedFlag = true;
			levelManager.totalScore += score;
		}
	}

}
