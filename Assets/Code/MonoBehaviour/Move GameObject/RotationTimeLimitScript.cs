﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTimeLimitScript : MonoBehaviour {

	public RotationScript rotationScript;

	public Timer timerClockwise, timerCounterclockwise;
	public Timer timerPauseAfterClockwise, timerPauseAfterCounterclockwise;

	public bool resetAngleBeforeClockwise, resetAngleBeforeCounterclockwise;
	public float angleBeforeClockwise, angleBeforeCounterclockwise;

	private bool clockwiseFlag;
	private float tmpScaleZ;
	private bool firstResetTimerClockwise = true, firstResetTimerCounterclockwise = true;

	public RotationStatusEnum rotationStatusEnum;

	private float zRotationScalePause;


	public void StartRotation() {
		timerClockwise.Start();
		timerCounterclockwise.Start();
		timerPauseAfterClockwise.Start();
		timerPauseAfterCounterclockwise.Start();
		rotationScript.zRotationScale = zRotationScalePause;
	}

	public void PauseRotation() {
		timerClockwise.Stop();
		timerCounterclockwise.Stop();
		timerPauseAfterClockwise.Stop();
		timerPauseAfterCounterclockwise.Stop();
		zRotationScalePause = rotationScript.zRotationScale;
		rotationScript.zRotationScale = 0;
	}




	public void Init() {
		if (rotationScript == null) {
			rotationScript = this.GetComponent<RotationScript>();
		}

		if (rotationScript.zRotationScale > 0) {
			clockwiseFlag = false;
			rotationStatusEnum = RotationStatusEnum.Counterclockwise;
		} else {
			clockwiseFlag = true;
			rotationStatusEnum = RotationStatusEnum.Clockwise;
		}
		zRotationScalePause = rotationScript.zRotationScale;
	}

	public RotationStatusEnum Rotation() {
		if (clockwiseFlag && timerClockwise.Check(false))
		{
			if (firstResetTimerClockwise)
			{
				timerPauseAfterClockwise.Reset();
				firstResetTimerClockwise = false;
				tmpScaleZ = rotationScript.zRotationScale;
				rotationScript.zRotationScale = 0;
				rotationStatusEnum = RotationStatusEnum.PauseAfterClockwise;
				if (resetAngleBeforeClockwise)
				{
					rotationScript.targetRigidbody.angularVelocity = Vector3.zero;
					rotationScript.targetRigidbody.rotation = Quaternion.Euler(rotationScript.targetRigidbody.rotation.eulerAngles.x,
																				rotationScript.targetRigidbody.rotation.eulerAngles.y,
																				angleBeforeClockwise);
				}
			}
			if (timerPauseAfterClockwise.Check(false))
			{
				rotationScript.zRotationScale = -tmpScaleZ;
				timerCounterclockwise.Reset();
				clockwiseFlag = false;
				firstResetTimerClockwise = true;
				rotationStatusEnum = RotationStatusEnum.Counterclockwise;
			}
		}
		else if (!clockwiseFlag && timerCounterclockwise.Check(false))
		{
			if (firstResetTimerCounterclockwise)
			{
				timerPauseAfterCounterclockwise.Reset();
				firstResetTimerCounterclockwise = false;
				tmpScaleZ = rotationScript.zRotationScale;
				rotationScript.zRotationScale = 0;
				rotationStatusEnum = RotationStatusEnum.PauseAfterCounterclockwise;
				if (resetAngleBeforeCounterclockwise)
				{
					rotationScript.targetRigidbody.angularVelocity = Vector3.zero;
					rotationScript.targetRigidbody.rotation = Quaternion.Euler(rotationScript.targetRigidbody.rotation.eulerAngles.x,
																				rotationScript.targetRigidbody.rotation.eulerAngles.y,
																				angleBeforeCounterclockwise);

				}
			}
			if (timerPauseAfterCounterclockwise.Check(false))
			{
				rotationScript.zRotationScale = -tmpScaleZ;
				timerClockwise.Reset();
				clockwiseFlag = true;
				firstResetTimerCounterclockwise = true;
				rotationStatusEnum = RotationStatusEnum.Clockwise;
			}
		}

		return rotationStatusEnum;
	}





	// Use this for initialization
	void Start () {
		Init();
	}
	
	// Update is called once per frame
	void Update () {
		Rotation();
	}


}




public enum RotationStatusEnum {
	none = 0,	//non si dovrebbe mai verificare

	Clockwise = 1,
	PauseAfterClockwise = 2,
	Counterclockwise = 3,
	PauseAfterCounterclockwise = 4

}