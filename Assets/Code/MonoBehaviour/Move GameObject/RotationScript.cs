﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour {

	/*/// <summary>
	/// TRUE: applica la rotazione al targetRigidbody
	/// FALSE: applica la rotazione al transform
	/// </summary>
	public bool haveRigidbody = false; */
	public Rigidbody targetRigidbody;
	//public Transform targetTransform;

	public SpeedAxis zRotation;

	public float zRotationScale;


	
	// Update is called once per frame
	void Update () {
		zRotation.Update(zRotationScale);
		if(targetRigidbody != null) {
			targetRigidbody.angularVelocity = new Vector3(0, 0, zRotation.actSpeed);
		} /*else if(targetTransform != null) {
			Debug.Log("asd2");
			float frameRotationSpeed = zRotation.actSpeed * Time.deltaTime;
			Vector3 zAxis = Vector3.forward;
			targetTransform.rotation.ToAngleAxis(out frameRotationSpeed, out zAxis);
		}*/
	}
}
