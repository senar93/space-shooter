﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script che permette a un oggetto di muoversi utilizando il movimento "standard";
/// utilizza DirectionSpeed per funzionare.
/// lo script che deciderà come si muove l'oggetto dovrà settare xScale e yScale come se stesse dando degli input un giocatore
/// </summary>
public class MovementScript : MonoBehaviour {

	/// <summary>
	/// velocità del giocatore sui 4 assi;
	/// se rb != null applica direttamente la velocità al Rigidbody
	/// </summary>
	public DirectionSpeed Speeds;
	/// <summary>
	/// intensità utilizzata per le velocità;
	/// nel caso del giocatore è = al input sui vari assi;
	/// DEVONO ESSERE SETTATE DA UN ALTRO SCRIPT
	/// </summary>
	public float xScale, yScale;



	// Use this for initialization
	void Start () {
		if(Speeds.targetRigidbody == null) {
			Speeds.targetRigidbody = this.GetComponent<Rigidbody>();
		}
	}
	

	void FixedUpdate() {
		Speeds.Update(xScale, yScale);
	}

}