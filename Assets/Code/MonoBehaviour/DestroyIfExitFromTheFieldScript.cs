﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyIfExitFromTheFieldScript : MonoBehaviour {
	public List<string> tagList;

	void OnTriggerExit(Collider other) {
		if(tagList.FindIndex(x => x == other.tag) >= 0)
			Destroy(other.gameObject);
	}

}
