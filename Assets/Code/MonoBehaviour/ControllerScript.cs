﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// controller del giocatore;
/// utilizza MovementScript per funzionare.
/// legge gli input inseriti e setta i parametri degli script in modo che eseguano le azioni corrispondenti
/// </summary>
public class ControllerScript : MonoBehaviour {

	public MovementScript movement;
	public ShooterScript shooter;

	public string xAxisName;
	public string yAxisName;
	public string primaryFireName;


	// Use this for initialization
	void Start () {
		if (movement == null) {
			movement = this.GetComponent<MovementScript>();
		}
		if (shooter == null) {
			shooter = this.GetComponent<ShooterScript>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		if(movement != null) {
			movement.xScale = Input.GetAxis(xAxisName);
			movement.yScale = Input.GetAxis(yAxisName);
		}
		if (shooter != null) {
			if(Input.GetAxis(primaryFireName) > 0) {
				shooter.Shoot();
			}
		}
	}

}
