﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagerScript : MonoBehaviour {

	public float totalScore = 0;
	public GameObject player;

	public string restartSceneName;
	public string[] sceneList;

	// Use this for initialization
	void Start () {
		if(player == null) {
			player = GameObject.FindGameObjectWithTag("Player");
		}
	}
	
	// Update is called once per frame
	void Update () {
		//restart
		if(Input.GetKeyDown(KeyCode.Escape)) {
			SceneManager.LoadScene(restartSceneName);
		}
		//pausa
		if(Input.GetKeyDown(KeyCode.P)) {
			if (Time.timeScale > 0) {
				Time.timeScale = 0;
			} else {
				Time.timeScale = 1;
			}	
		}

		if(Input.GetKeyDown(KeyCode.F1)) {
			SceneManager.LoadScene(sceneList[0]);
		}/* else if(Input.GetKeyDown(KeyCode.F2)) {
			SceneManager.LoadScene(sceneList[1]);
		}*/	//altre scene di test
	}
}
