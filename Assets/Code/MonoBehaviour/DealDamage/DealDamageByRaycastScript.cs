﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamageByRaycastScript : MonoBehaviour {

	public float dps;
	public float rayCastLenght = 500;
	public Transform startOfLine;
	public Vector3 direction;
	
	//public Timer damageTimer;
	//public Timer checkTimer;

	public List<string> targetTagList;
	public List<string> layerMaskList;

	private int layerMaskNumber;
	private RaycastHit hitTarget;


	public void UpdateLayerMask() {
		layerMaskNumber = LayerMask.GetMask(layerMaskList.ToArray());
	}



	private void Start() {
		if(startOfLine == null) {
			startOfLine = this.transform;
		}
		UpdateLayerMask();
		//checkTimer.Reset();
	}


	private void FixedUpdate() {
		if (Physics.Raycast(startOfLine.position, direction, out hitTarget, rayCastLenght, layerMaskNumber)) {
			if(targetTagList.FindIndex(x => x == hitTarget.collider.tag) >= 0) {
				Debug.Log("Hit " + hitTarget.transform.name);
				HealthScript tmpTargetHp = hitTarget.collider.GetComponent<HealthScript>();
				if(tmpTargetHp != null) {
					tmpTargetHp.DealDamage(dps * Time.deltaTime);
				}
			}
		}
	}

}
