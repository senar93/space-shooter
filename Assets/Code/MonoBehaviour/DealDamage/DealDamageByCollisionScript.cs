﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamageByCollisionScript : MonoBehaviour {

	public float damage;
	public List<string> targetTag;



	void OnTriggerEnter(Collider other) {
		if(targetTag.FindIndex(x => x == other.gameObject.tag) >= 0) {
			HealthScript targetHp = other.gameObject.GetComponent<HealthScript>();
			if(targetHp != null)
				targetHp.DealDamage(damage);
			Destroy(this.gameObject);
		}
	}


}
