﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript_Pattern0 : MonoBehaviour {

	public MovementScript movement;
	public List<ShooterScript> shooters;

	public bool xEnableBounce, yEnableBounce;
	public float xBounceMin, xBounceMax;
	public float yBounceMin, yBounceMax;



	// Use this for initialization
	void Start () {
		if (movement == null)
			movement = GetComponent<MovementScript>();
		if (shooters.Count == 0)
			shooters = new List<ShooterScript>(GetComponents<ShooterScript>());
		
	}
	

	// Update is called once per frame
	void FixedUpdate () {
		if (movement != null) {
			//rimbalzo su x
			if (xEnableBounce) {
				if (transform.position.x <= xBounceMin) {
					movement.xScale = 1;
				}
				else if (transform.position.x >= xBounceMax) {
					movement.xScale = -1;
				}
			}

			//rimbalzo su y
			if (yEnableBounce) {
				if (transform.position.y <= yBounceMin) {
					movement.yScale = 1;
				}
				else if (transform.position.y >= yBounceMax) {
					movement.yScale = -1;
				}
			}
		}

	}


	void Update() {
		if(shooters != null) {
			for(int i = 0; i < shooters.Count; i++) {
				if(shooters[i] != null) {
					shooters[i].Shoot();
				}
			}
		}

	}



}
