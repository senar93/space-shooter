﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// script che contgiene gli hp di un oggetto, permette di danneggiarlo e, se destroyIfHealthBecome0 = true, di distruggerlo
/// </summary>
public class HealthScript : MonoBehaviour {

	/// <summary>
	/// se TRUE l'oggetto viene distrutto quando la sua vita raggiunge lo 0, altrimenti no
	/// </summary>
	public bool destroyIfHealthBecome0 = true;	

	/// <summary>
	/// vita attuale del oggetto, compresa tra 0 e maxHealth
	/// </summary>
	public float actHealth;
	/// <summary>
	/// vita massima del oggetto
	/// </summary>
	public float maxHealth;

	public HaveScoreScript scoreScript;


	// Use this for initialization
	void Start () {
		actHealth = Mathf.Clamp(actHealth, 0f, maxHealth);
		if(scoreScript == null) {
			scoreScript = this.GetComponent<HaveScoreScript>();
		}
	}
	
	/// <summary>
	/// infligge un certo ammontare di danni all oggetto
	/// </summary>
	/// <param name="value"></param>
	public void DealDamage(float value) {
		actHealth = Mathf.Clamp(actHealth - value, 0f, maxHealth);
		if(actHealth == 0) {
			if(scoreScript != null) {
				scoreScript.AddScore();
			}
			if(destroyIfHealthBecome0) {
				Destroy(this.gameObject);
			}
		}
	}

}
