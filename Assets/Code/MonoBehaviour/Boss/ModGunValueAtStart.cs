﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModGunValueAtStart : MonoBehaviour {
	public ShootingScript shootingScript;

	public bool destroyAtStart = true;
	public float changeShootSpeed = 0.075f;
	public int changeBulletNumber = 1;

	// Use this for initialization
	void Start () {
		shootingScript.shootCD.setPart -= changeShootSpeed;
		shootingScript.shootNumber += changeBulletNumber;
		if(destroyAtStart) {
			Destroy(this.gameObject);
		}
	}

}
