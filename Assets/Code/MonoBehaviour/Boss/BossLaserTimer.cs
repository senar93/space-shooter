﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLaserTimer : MonoBehaviour {

	public RotationTimeLimitScript[] laserRays;
	//public Timer[] statusMachine;
	[SerializeField] private int statusLenght = 4;
	[SerializeField] private int currentStatus;
	private bool bossStarting;
	[SerializeField] private bool[] flagToDisable = { false, false, false, false };



	public void StartBoss() {
		bossStarting = true;
		laserRays[0].PauseRotation();
		laserRays[1].PauseRotation();
		laserRays[2].PauseRotation();
		laserRays[3].PauseRotation();
	}
	

	private void NextStatus() {
		//update currentStatus
		if(currentStatus >= statusLenght) {
			currentStatus = 0;
		} else {
			currentStatus++;
		}
	}


	public void UpdateStatus() {
		//abilita i laser in modo che partano al momento opportuno
		switch (currentStatus) {
			case 0:         //attiva laser 00		1,0,0,1
				if (laserRays[3].rotationStatusEnum == RotationStatusEnum.PauseAfterClockwise || bossStarting) {
					laserRays[0].StartRotation();
					flagToDisable[0] = false;
					flagToDisable[3] = true;
					if (bossStarting)
						flagToDisable[3] = false;
					bossStarting = false;
					NextStatus();
				}
				break;
			case 1:         //attiva laser 01		1,1,0,0
				if (laserRays[0].rotationStatusEnum == RotationStatusEnum.PauseAfterCounterclockwise) {
					laserRays[1].StartRotation();
					flagToDisable[1] = false;
					flagToDisable[0] = true;
					NextStatus();
				}
				break;
			case 2:         //attiva laser 02		0,1,1,0
				if (laserRays[1].rotationStatusEnum == RotationStatusEnum.PauseAfterClockwise) {
					laserRays[2].StartRotation();
					flagToDisable[2] = false;
					flagToDisable[1] = true;
					NextStatus();
				}
				break;
			case 3:         //attiva laser 03		0,0,1,1
				if (laserRays[2].rotationStatusEnum == RotationStatusEnum.PauseAfterCounterclockwise) {
					laserRays[3].StartRotation();
					flagToDisable[3] = false;
					flagToDisable[2] = true;
					NextStatus();
				}
				break;
			// riparte
		}

		//disabilita i laser al termine della corsa se sono flaggati
		if (flagToDisable[0] && laserRays[0].rotationStatusEnum == RotationStatusEnum.Counterclockwise) {
			laserRays[0].PauseRotation();
			flagToDisable[0] = false;
		}
		if (flagToDisable[1] && laserRays[1].rotationStatusEnum == RotationStatusEnum.Clockwise) {
			laserRays[1].PauseRotation();
			flagToDisable[1] = false;
		}
		if (flagToDisable[2] && laserRays[2].rotationStatusEnum == RotationStatusEnum.Counterclockwise) {
			laserRays[2].PauseRotation();
			flagToDisable[2] = false;
		}
		if (flagToDisable[3] && laserRays[3].rotationStatusEnum == RotationStatusEnum.Clockwise) {
			laserRays[3].PauseRotation();
			flagToDisable[3] = false;
		}

	}




	void Start() {
		StartBoss();
	}


	// Update is called once per frame
	void Update () {
		//NextStatus();
		//Debug.Log(currentStatus);
		UpdateStatus();
	}


}
