﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScoreAtStart : MonoBehaviour {

	public HaveScoreScript scoreScript;
	public GameObject objThatGiveScore;

	public bool destroyAtStart = true;
	public bool destroyGameObjectThatGiveScore = true;


	// Use this for initialization
	void Start () {
		if(scoreScript == null) {
			scoreScript = this.GetComponent<HaveScoreScript>();
		}

		scoreScript.AddScore();

		if(destroyGameObjectThatGiveScore) {
			Destroy(objThatGiveScore);
		}
		if (destroyAtStart) {
			Destroy(this.gameObject);
		}

	}

}
