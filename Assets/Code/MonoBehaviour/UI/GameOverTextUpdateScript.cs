﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameOverTextUpdateScript : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start() {
		if (player == null) {
			player = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManagerScript>().player;
		}
		this.GetComponent<Text>().text = "";
	}

	// Update is called once per frame
	void Update() {
		if(player == null) {
			this.GetComponent<Text>().text = "GAME OVER";
		}
	}

}
