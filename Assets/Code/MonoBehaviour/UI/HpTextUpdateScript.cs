﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpTextUpdateScript : MonoBehaviour {

	public HealthScript player;

	// Use this for initialization
	void Start () {
		if(player == null) {
			player = GameObject.FindGameObjectWithTag("Player").GetComponent<HealthScript>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<Text>().text = "Hp: " + Mathf.RoundToInt(player.maxHealth) + " / " + Mathf.RoundToInt(player.actHealth);
	}
}
