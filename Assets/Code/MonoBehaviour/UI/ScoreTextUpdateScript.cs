﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextUpdateScript : MonoBehaviour {

	public LevelManagerScript levelManager;

	// Use this for initialization
	void Start() {
		if (levelManager == null) {
			levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManagerScript>();
		}
	}

	// Update is called once per frame
	void Update() {
		this.GetComponent<Text>().text = "Score: " + Mathf.RoundToInt(levelManager.totalScore);
	}

}
