﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// script che permette a un oggetto di sparare un proiettile in una certa posizione con una certa rotazione e con un certo fireRate
/// </summary>
[System.Serializable] public class ShooterScript : MonoBehaviour{

	/// <summary>
	/// identifica se l'oggetto può sparare o meno
	/// </summary>
	public bool canShoot = true;

	/// <summary>
	/// prefab del proiettile che verrà sparato
	/// </summary>
	public GameObject bulletPrefab;
	/// <summary>
	/// punto di spawn del proiettile;
	/// il proiettile prende la posizione e la rotazione del punto di spawn
	/// </summary>
	public Transform spawnPoint;
	/// <summary>
	/// timer contenente il rateo di fuoco del oggetto
	/// </summary>
	public Timer fireRate;

	/*/// <summary>
	/// imposta automaticamente la scala sulla velocità X in modo che il proiettile si muova in diagonale coerentemente con la rotazione
	/// </summary>
	public bool setXScaleByRotationAngle = true;
	*/
	public float customScaleX, customScaleY;


	/// <summary>
	/// identifica se l'oggetto deve sparare o meno nel prossimo Update che verrà eseguito
	/// </summary>
	protected bool shooting = false;



	/// <summary>
	/// se è possibile sparare comunica all oggetto di sparare nel Update di questo script successivo
	/// </summary>
	/// <returns>TRUE: se è avvenuto lo sparo</returns>
	public bool Shoot() {
		if(canShoot && fireRate.Check(false)) {
			shooting = true;
			return true;
		} else {
			return false;
		}
	}


	/// <summary>
	/// se shooting = TRUE spara il proiettile
	/// </summary>
	protected void Update() {
		if(shooting) {
			fireRate.Reset();
			shooting = false;
			InstantiatePrefab();
		}
	}


	/// <summary>
	/// istanzia il prefab del proiettile settandone i parametri
	/// </summary>
	/// <returns>puntatore al proiettile</returns>
	protected GameObject InstantiatePrefab() {
		GameObject tmpObj = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation) as GameObject;

		/*if (setXScaleByRotationAngle) {
			tmpObj.GetComponent<MovementScript>().xScale = tmpObj.GetComponent<MovementScript>().yScale * Mathf.Sin(tmpObj.transform.rotation.z) * Mathf.PI;
			Debug.Log(Mathf.Sin(tmpObj.transform.rotation.z) * Mathf.PI/2);
		}*/
		if(customScaleX != 0 || customScaleY != 0) {
			tmpObj.GetComponent<MovementScript>().xScale = customScaleX;
			tmpObj.GetComponent<MovementScript>().yScale = customScaleY;
		}


		return tmpObj;
	}


}
