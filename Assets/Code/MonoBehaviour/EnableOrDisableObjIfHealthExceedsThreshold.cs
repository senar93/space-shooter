﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOrDisableObjIfHealthExceedsThreshold : MonoBehaviour {

	public bool destroyScriptIfConditionVerified = true;

	/// <summary>
	/// TRUE: abilità l'oggetto target
	/// FALSE: disabilita l'oggetto target
	/// </summary>
	public bool enableActionFlag = true;

	/// <summary>
	/// TRUE: controlla se gli hp sono minori della soglia
	/// FALSE: controlla se gli hp sono maggiori della soglia
	/// </summary>
	public bool[] dropCheckFlag;
	public float[] hpThreshold;

	/// <summary>
	/// TRUE: setta il target se tutti i valori di hp sono oltre la soglia stabilita
	/// FALSE: setta il target se almeno 1 valore di hp è sotto la soglia stabilita
	/// </summary>
	public bool andFlag = true;
	public HealthScript[] objHp;
	public GameObject target;

	private bool checkResult;
	


	// Update is called once per frame
	void Update () {
		checkResult = (andFlag) ? true : false;
		for(int i = 0; i < objHp.Length; i++) {
			if (dropCheckFlag[i] && objHp[i].actHealth <= hpThreshold[i]) {
				if(!andFlag) {
					checkResult = true;
					break;
				}
			}
			else if (!dropCheckFlag[i] && objHp[i].actHealth > hpThreshold[i]) {
				if(!andFlag) {
					checkResult = true;
					break;
				}
			}
			else if(andFlag) {            //la vita da controllare non supera la soglia
				checkResult = false;
				break;
			}
		}
		if(checkResult) {
			target.SetActive(enableActionFlag);
			if(destroyScriptIfConditionVerified) {
				Destroy(this);
			}
		}

	}

}
