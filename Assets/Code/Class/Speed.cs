﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// classe che rappresenta la velocità su un asse di un oggetto.
/// può essere lineare o non lineare
/// per aggiornare il valore di velocità si richiama il suo metodo Update(scale) passandogli l'intensità del input
/// </summary>
[System.Serializable]
public class SpeedAxis {
	/// <summary>
	/// abilità la velocità su una certa asse
	/// </summary>
	public bool enabled;
	/// <summary>
	/// TRUE: imposta actSpeed ogni volta che viene richiamata Update al valore di acceleration * scale
	/// FALSE: utilizza i normali valori di accelerazione, decellerazione ecc
	/// </summary>
	public bool isLinear = true;

	/// <summary>
	/// velocità attuale
	/// </summary>
	[SerializeField] private float _actSpeed;
	/// <summary>
	/// velocità minima raggiungibile
	/// </summary>
	public float minSpeed;
	/// <summary>
	/// velocità massima raggiungibile
	/// </summary>
	public float maxSpeed;
	/// <summary>
	/// accelerazione utilizzata con scale maggiore di 0
	/// </summary>
	public float acceleration;
	/// <summary>
	/// accelerazione utilizzata con scale minore di 0
	/// non usato se isLinear = FALSE
	/// </summary>
	public float deceleration;

	/// <summary>
	/// minimo valore di velocità (+0 di default)
	/// </summary>
	public float zeroValuePositive = 0;
	/// <summary>
	/// minimo valore di velocità (-0 di default)
	/// </summary>
	public float zeroValueNegative = 0;
	/// <summary>
	/// decelerazione usata per riportare actSpeed a 0 se non si sta fornendo alcun input
	/// non usato se isLinear = FALSE
	/// </summary>
	public float returnToZeroDecelleration;


	public float actSpeed {
		get {
			if (enabled) {
				return _actSpeed;
			} else {
				return 0;
			}
		}

		set {
			_actSpeed = value;
		}
	}




	public void Update(float scale) {
		if (enabled) {
			if (isLinear) {
				LinearMotion(scale);
			}
			else {
				NotLinearMotion(scale);
			}

			_actSpeed = Mathf.Clamp(_actSpeed, minSpeed, maxSpeed);
		}
	}


	private void LinearMotion(float scale) {
		_actSpeed = scale * acceleration;

		if (_actSpeed > 0 && _actSpeed < zeroValuePositive) {
			_actSpeed = zeroValuePositive;
		} else if (actSpeed < 0 && _actSpeed > zeroValueNegative) {
			_actSpeed = zeroValueNegative;
		}
	}

	private void NotLinearMotion(float scale) {
		if(scale == 0) {
			if(_actSpeed > 0) {
				_actSpeed = Mathf.Clamp(_actSpeed - (returnToZeroDecelleration * Time.deltaTime), zeroValuePositive, maxSpeed);
			} else if(_actSpeed < 0) {
				_actSpeed = Mathf.Clamp(_actSpeed + (returnToZeroDecelleration * Time.deltaTime), minSpeed, zeroValueNegative);
			}
		} else if(scale > 0) {
			_actSpeed += scale * acceleration * Time.deltaTime;
		}
		else if(scale < 0) {
			_actSpeed -= deceleration * Time.deltaTime;
		}

	}

}