﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class XY {
	public float up, down, right, left;
}