﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnInfo {
	public GameObject prefab;
	public Transform positon;
	public Timer timer;

	public float customScaleX, customScaleY;
}