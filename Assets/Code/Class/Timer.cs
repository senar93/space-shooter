﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/// <summary>
/// classe contenente un timer con una parte fissa e una casuale
/// </summary>
[System.Serializable] public class Timer {
	
	/// <summary>
	/// parte fissa del timer
	/// </summary>
	public float setPart;
	/// <summary>
	/// valore minimo della parte casuale 
	/// </summary>
	public float randomPartRangeMin;
	/// <summary>
	/// valore massimo della parte casuale
	/// </summary>
	public float randomPartRangeMax;

	/// <summary>
	/// parte casuale del timer, ricalcolata ogni volta che Check() restitusice true
	/// </summary>
	[HideInInspector] public float randomPart;
	/// <summary>
	/// ultimo tempo in cui Check() ha restituito true, ricalcolato ogni volta che Check() restitusice true
	/// </summary>
	[HideInInspector] public float lastTime;

	private bool stopFlag = false;


	/// <summary>
	/// costruttore della classe Timer
	/// </summary>
	/// <param name="setPart"></param>
	/// <param name="randomPartRangeMin"></param>
	/// <param name="randomPartRangeMax"></param>
	/// <param name="nextTime"></param>
	public Timer(float setPart, float randomPartRangeMin, float randomPartRangeMax, float nextTime = 0) {
		this.setPart = setPart;
		this.randomPartRangeMin = randomPartRangeMin;
		this.randomPartRangeMax = randomPartRangeMax;
		Reset();
	}

	/// <summary>
	/// controlla se il timer è scaduto o meno;
	/// se resetTimer = true dopo aver controllato resetta anche il timer
	/// </summary>
	/// <param name="resetTimer">TRUE: resetta il timer dopo il controllo (facendo un Check() l'fps successivo probabilmente darà falso)</param>
	/// <returns>TRUE: il timer è scaduto</returns>
	public bool Check(bool resetTimer = false) {
		if(!stopFlag && lastTime + setPart + randomPart <= Time.time) {
			if(resetTimer) {
				Reset();
			}

			return true;
		}

		return false;
	}
	
	/// <summary>
	/// resetta il timer con i valori del fps corrente
	/// </summary>
	public void Reset() {
		randomPart = Random.Range(randomPartRangeMin, randomPartRangeMax);
		Start();
	}

	public void Stop() {
		stopFlag = true;
	}

	public void Start() {
		lastTime = Time.time;
		stopFlag = false;
	}


}