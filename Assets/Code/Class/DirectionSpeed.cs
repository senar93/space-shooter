﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// rappresenta la velocità di un oggetto "standard" tipo il giocatore, un nemico o un proiettile;
/// utilizza la classe SpeedAxis per funzionare.
/// se viene settato un targetRigidbody quando si richiama Update(...) oltre a settare i valori delle velocità sugli assi le imposta direttamente nel Rigidbody;
/// può abilitare anche delle limitazioni alle coordinate dell oggetto (bound);
/// </summary>
[System.Serializable]
public class DirectionSpeed {
	public Rigidbody targetRigidbody;

	public SpeedAxis x;
	public SpeedAxis y;

	public bool isBound = true;
	public XY bound;

	public void Update(float xScale, float yScale) {
		UpdateSpeed(xScale, yScale);
		UpdateRbVelocity();
		BoundRigidbodyPosition();
	}


	private void UpdateSpeed(float xScale, float yScale) {
		x.Update(xScale);
		y.Update(yScale);
	}

	private void UpdateRbVelocity() {
		if (targetRigidbody != null) {
			targetRigidbody.velocity = new Vector3(x.actSpeed, y.actSpeed, 0f);
		}
	}

	private void BoundRigidbodyPosition() {
		if(isBound) {
			targetRigidbody.position = new Vector3 (Mathf.Clamp(targetRigidbody.position.x, bound.left, bound.right),
											  Mathf.Clamp(targetRigidbody.position.y, bound.down, bound.up),
											  0.0f);
		}
	}

}